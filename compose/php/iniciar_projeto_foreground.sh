#!/bin/sh

chown -R www-data:www-data /var/www/html/patrimonio/storage
chown -R www-data:www-data /var/www/html/patrimonio/bootstrap/cache
chmod -R ugo+rw /var/www/html/patrimonio/storage
chmod -R ugo+rw /var/www/html/patrimonio/bootstrap/cache
chmod -R ugo+rw  /var/www/html/patrimonio/storage/logs/.

# echo "Limpar cache ..." >> $NOMEARQUIVO
php -f /var/www/html/patrimonio/artisan optimize:clear
php -f /var/www/html/patrimonio/artisan l5-swagger:generate

# echo "Baixar as novas branches ..." >> $NOMEARQUIVO
git fetch --all

sed -i 's/APP_AMB=.*/APP_AMB="Ambiente Desenvolvimento"/' /var/www/html/patrimonio/.env

service cron start

# echo "Iniciar o apache ..." >> $NOMEARQUIVO
#apachectl start
# apachectl -D FOREGROUND
/usr/sbin/apache2ctl -D FOREGROUND
