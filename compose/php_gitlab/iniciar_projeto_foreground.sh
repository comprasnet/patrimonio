#!/bin/sh

php -f /var/www/html/patrimonio/artisan migrate --force
php -f /var/www/html/patrimonio/artisan cache:clear
php -f /var/www/html/patrimonio/artisan config:cache
php -f /var/www/html/patrimonio/artisan l5-swagger:generate


chmod -R ugo+rw /var/www/html/patrimonio/storage
chmod -R ugo+rw /var/www/html/patrimonio/bootstrap/cache

service cron start

/usr/sbin/apache2ctl -D FOREGROUND
