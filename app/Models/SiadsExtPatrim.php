<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiadsExtPatrim extends Model
{
    use HasFactory;

    protected $connection='pgsql2';
    protected $table='siads_ext_patrim';

    public function getPatrimonioApiComFiltro(Request $request)
    {
        if(!$request->offset){
            $dados = $this->skip(0)->take(1000);
        }else{
            $dados = $this->skip($request->offset)->take(1000);
        }

        if($request->orgao){
            $dados->where('it_co_orgao',$request->orgao)->orderBy('it_co_ug','ASC');
        }

        if($request->unidade){
            $dados->where('it_co_ug',$request->unidade)->orderBy('it_co_uorg','ASC');
        }

        if($request->uorg){
            $dados->where('it_co_uorg',$request->uorg)->orderBy('it_nu_patrimonial','ASC');
        }

        if($request->numero_patrimonio){
            $dados->where('it_nu_patrimonial',$request->numero_patrimonio);
        }

        $dados->select(
            DB::Raw('it_co_orgao::varchar as orgao'),
            DB::Raw("LPAD(it_co_ug::varchar,6,'0') as unidade"),
            DB::Raw("LPAD(it_co_gestao::varchar,5,'0') as gestao"),
            DB::Raw("trim(it_co_uorg::varchar) as uorg"),
            DB::Raw('it_co_conta_contabil::bigint as conta_contabil'),
            DB::Raw('it_nu_patrimonial_ant::bigint as numero_patrimonial_antigo'),
            DB::Raw('it_nu_patrimonial::bigint as numero_patrimonial'),
            DB::Raw("(substring(it_da_tombamento::varchar,1,4) ||'-'|| substring(it_da_tombamento::varchar,5,2) ||'-'|| substring(it_da_tombamento::varchar,7,2))::date as data_tombamento"),
            DB::Raw('it_co_item_material::varchar as codigo_material'),
            DB::Raw('TRIM(it_no_material::varchar) as nome_material'),
            DB::Raw('trim(it_tx_descricao_complementar::varchar) as descricao_complementar'),
            DB::Raw('trim(it_no_marca::varchar) as marca'),
            DB::Raw('trim(it_no_modelo::varchar) as modelo'),
            DB::Raw('trim(it_nu_serie::varchar) as serie'),
            DB::Raw('trim(it_no_fabricante::varchar) as fabricante'),
            DB::Raw('trim(it_nu_processo::varchar) as processo'),
            DB::Raw('trim(it_co_terceiros::varchar) as codigo_terceiros'),
            DB::Raw('round(it_va_inicial_bem::numeric,2) as valor_inicial_bem'),
            DB::Raw('round(it_va_bem::decimal,2) as valor_bem'),
            DB::Raw('round(it_va_bem_depre::decimal,2) as valor_depreciado'),
            DB::Raw('round(it_va_liq_bem::decimal,2) as valor_liquido'),
            DB::Raw('round(it_va_doacao_venda::decimal,2) as valor_doacao_venda'),
            DB::Raw('trim(it_co_tipo_bem::varchar) as codigo_tipo_bem'),
            DB::Raw('trim(it_no_tipo_bem::varchar) as nome_tipo_bem'),
            DB::Raw('trim(it_co_situacao::varchar) as codigo_situacao'),
            DB::Raw('trim(it_no_situacao::varchar) as nome_situacao'),
            DB::Raw('trim(it_co_destinacao::varchar) as codigo_destinacao'),
            DB::Raw('trim(it_no_destinacao::varchar) as nome_destinacao'),
            DB::Raw('it_nu_cpf_responsavel::bigint as cpf_responsavel'),
            DB::Raw('trim(it_no_responsavel::varchar) as nome_responsavel'),
            DB::Raw('it_co_resp_uorg::bigint as cpf_responsavel_uorg'),
            DB::Raw('trim(it_no_resp_uorg::varchar) as nome_responsavel_uorg'),
        );

        return $dados->orderBy('it_nu_patrimonial')->get()->toArray();
    }
}
