<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\SiadsExtPatrim;
use Illuminate\Http\Request;

class PatrimonioController extends Controller
{
    public function buscaSiadsPatrimonioComFiltro(Request $request)
    {
        $dados = new SiadsExtPatrim();

        return response()->json($dados->getPatrimonioApiComFiltro($request));
    }
}
